import React, { useMemo } from "react";
import { Layout } from "../features/layout/components/Layout";
import { selectSearch } from "../features/layout/searchSlice";
import { MoviesListGrid } from "../features/movies/components/MoviesListGrid";
import {
  useGetPopularMoviesQuery,
  useSearchMoviesByTitleQuery,
} from "../features/movies/services";
import { useAppSelector } from "../store";

export const MoviesListPage: React.FC = () => {
  const { data, error, isLoading } = useGetPopularMoviesQuery();
  const searchText = useAppSelector(selectSearch);
  const { data: searchResult } = useSearchMoviesByTitleQuery(searchText, {
    skip: !searchText,
  });

  const movies = useMemo(() => searchResult?.results || data?.results, [searchResult, data]);

  return (
    <Layout
      backgroundImgSrc="/images/movies.jpg"
      searchText={searchText}
      searchResultTotal={movies?.length}
    >
      <MoviesListGrid
        isLoading={isLoading}
        error={error}
        movies={movies}
      />
    </Layout>
  );
};
