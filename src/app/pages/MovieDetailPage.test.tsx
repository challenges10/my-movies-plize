import React from "react";
import { render, screen, waitFor } from "@testing-library/react";
import { useParams, useLocation } from "react-router-dom";
import { useGetMovieDetailQuery } from "../features/movies/services";
import { MoviesDetailPage } from "./MovieDetailPage";
import { renderWithContext } from "../../test-utils";

jest.mock("react-router-dom", () => ({
  ...jest.requireActual("react-router-dom"),
  useParams: jest.fn(),
  useLocation: jest.fn(),
}));

jest.mock("../features/movies/services", () => ({
  ...jest.requireActual("../features/movies/services"),
  useGetMovieDetailQuery: jest.fn(),
}));

describe("MoviesDetailPage component", () => {
  it("renders correctly with a movie detail", () => {
    (useParams as jest.Mock).mockReturnValue({ movieId: "123" });
    (useGetMovieDetailQuery as jest.Mock).mockReturnValue({
      data: {
        id: 123,
        title: "Movie Title",
        backdrop_path: "/path/to/backdrop",
      },
      error: false,
      isLoading: false,
    });

    renderWithContext(<MoviesDetailPage />);
  });

  it("renders a loading state", async () => {
    (useParams as jest.Mock).mockReturnValue({ movieId: "123" });
    (useGetMovieDetailQuery as jest.Mock).mockReturnValue({
      data: undefined,
      error: undefined,
      isLoading: true,
    });

    renderWithContext(<MoviesDetailPage />);
    expect(screen.getByText("Loading...")).toBeInTheDocument()
  });

  it("renders an error state", () => {
    (useParams as jest.Mock).mockReturnValue({ movieId: "123" });
    (useGetMovieDetailQuery as jest.Mock).mockReturnValue({
      data: undefined,
      error: true,
      isLoading: false,
    });
    renderWithContext(<MoviesDetailPage />);
    expect(screen.getByText("Page not found")).toBeInTheDocument();
  });
});
