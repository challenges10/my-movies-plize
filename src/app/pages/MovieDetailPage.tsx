import { useParams } from "react-router-dom";
import { useGetMovieDetailQuery } from "../features/movies/services";
import { getImageUrl } from "../features/movies/utils";
import { MovieDetail } from "../features/movies/components/MovieDetail";
import { Layout } from "../features/layout/components/Layout";

export const MoviesDetailPage: React.FC = () => {
  const { movieId } = useParams();

  const {
    data: movie,
    error,
    isLoading,
  } = useGetMovieDetailQuery(movieId || "");

  if (error) {
    return <div>Page not found </div>;
  }

  if (isLoading || !movie) return <div>Loading...</div>;

  return (
    <Layout
      backgroundImgSrc={getImageUrl(movie.backdrop_path || "", "w1280")}
      hasHeader={false}
    >
      <MovieDetail movie={movie} />
    </Layout>
  );
};
