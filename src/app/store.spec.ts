import { store } from "./store";

describe('Store', () => {
  it('should create store', () => {
    expect(Object.keys(store.getState())).toEqual([
      "search",
      'moviesApi',
    ]);
  });
});
