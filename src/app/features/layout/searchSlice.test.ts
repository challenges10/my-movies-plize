
import searchSlice, { selectSearch, setSearchText } from "./searchSlice";
describe('Search Slice', () => {
  it('should create search slice', () => {
    const initialState = undefined
    const action = setSearchText("test")
    const result = searchSlice(initialState, action)
    expect(result).toEqual({ searchText: "test" })
  });
});

describe('selectSearch', () => {
  it('selects search text', () => {
    const state: any = {
      search: {
        searchText: 'hello',
      },
    };
    const searchText = selectSearch(state);
    expect(searchText).toEqual('hello');
  });
});