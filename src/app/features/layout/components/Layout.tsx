import { PropsWithChildren } from "react";
import { Background } from "./Background";
import { Footer } from "./Footer";
import { Header } from "./Header";
import { SearchResult, SearchResultProps } from "./Header/SearchResult";
import { useAppDispatch } from "../../../store";
import { setSearchText } from "../searchSlice";

type LayoutProps = {
  backgroundImgSrc?: string;
  hasHeader?: boolean;
  searchText?: string;
} & SearchResultProps &
  PropsWithChildren;

export const Layout: React.FC<LayoutProps> = ({
  backgroundImgSrc,
  hasHeader = true,
  searchText = "",
  searchResultTotal,
  children,
}) => {

  const dispatch = useAppDispatch();
  const handleOnClearSearch = () => {
    dispatch(setSearchText(""));
  };

  const handleOnSearch = (value: string) => {
    dispatch(setSearchText(value));
  };
  return (
    <div className="relative min-h-screen flex flex-col justify-between">
      {backgroundImgSrc && <Background imgSrc={backgroundImgSrc} />}
      {hasHeader && (
        <Header searchInputValue={searchText} onSearch={handleOnSearch} />
      )}

      <main className="container flex-1">
        {hasHeader && searchResultTotal && (
          <SearchResult
            onClearSearch={handleOnClearSearch}
            searchResultTotal={searchResultTotal}
            searchText={searchText}
          />
        )}
        {children}
      </main>

      <Footer />
    </div>
  );
};
