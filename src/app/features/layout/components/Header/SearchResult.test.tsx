import React from 'react';
import { render, screen } from '@testing-library/react';
import { SearchResult, SearchResultProps } from './SearchResult';

describe('SearchResult', () => {
  const defaultProps: SearchResultProps = {
    searchText: 'test',
    searchResultTotal: 10,
  };

  it('should render the component with the correct props', () => {
    render(<SearchResult {...defaultProps} />);
    const searchTextNode = screen.getByText('test (10)');
    const searchResultLabelNode = screen.getByText('Search results :');
    const clearButtonNode = screen.getByText('Clear');

    expect(searchTextNode).toBeInTheDocument();
    expect(searchResultLabelNode).toBeInTheDocument();
    expect(clearButtonNode).toBeInTheDocument();
  });

  it('should call the onClearSearch prop when the clear button is clicked', () => {
    const onClearSearch = jest.fn();
    const props = { ...defaultProps, onClearSearch };
    render(<SearchResult {...props} />);
    const clearButtonNode = screen.getByText('Clear');

    clearButtonNode.click();

    expect(onClearSearch).toHaveBeenCalled();
  });
});
