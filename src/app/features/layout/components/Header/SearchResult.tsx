import { PropsWithChildren } from "react";

export type SearchResultProps = {
  onClearSearch?: () => void;
  searchText?: string;
  searchResultTotal?: number;
} & PropsWithChildren;

export const SearchResult: React.FC<SearchResultProps> = ({
  searchText = "",
  searchResultTotal,
  onClearSearch,
}) => {
  return (
    <div className="flex pt-4 items-center justify-between text-[#ffffff7c] ">
      <h5 className="font-bold text-base">
        Search results :
        <span className="ml-2 px-2 underline underline-offset-1 rounded-md">
          {searchText} ({searchResultTotal})
        </span>
      </h5>
      <button
        onClick={onClearSearch}
        className="border border-[#324077] hover:bg-[#324077] hover:text-white px-4 py-2 rounded-md text-sm uppercase font-bold transition-all"
      >
        Clear
      </button>
    </div>
  );
};
