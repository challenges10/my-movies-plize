import { Link } from "react-router-dom";
import { Input } from "antd";
import { useEffect, useState } from "react";

type HeaderProps = {
  searchInputValue?: string;
  onSearch?: (value: string) => void;
};

export const Header: React.FC<HeaderProps> = ({
  searchInputValue = "",
  onSearch,
}) => {
  const [inputValue, setInputValue] = useState<string>("");
  const handleOnSearch = (value: string) => {
    if (onSearch) {
      onSearch(value);
    }
  };

  const handleInputChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    setInputValue(event.target.value);
  };

  useEffect(() => {
    setInputValue(searchInputValue);
  }, [searchInputValue]);

  return (
    <header className="bg-[#324077e0] h-[72px] sticky top-0 z-20 flex items-center">
      <div className="container flex justify-between items-center">
        <Link
          to=""
          className="text-white text-xl md:text-4xl italic font-extrabold whitespace-nowrap"
        >
          My Movies
        </Link>
        <div className="bg-white rounded-md max-w-[60%] md:max-w-none">
          <Input.Search
            placeholder="Search title..."
            size="large"
            onSearch={handleOnSearch}
            onChange={handleInputChange}
            value={inputValue}
          />
        </div>
      </div>
    </header>
  );
};
