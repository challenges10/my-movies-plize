export const Background: React.FC<{ imgSrc: string }> = ({ imgSrc }) => {
  return (
    <div className="fixed h-screen w-screen -z-10">
      <img src={imgSrc} alt="background" className="w-full h-full bg-cover" />
      <div className="absolute top-0 left-0 bg-[#000000b9] w-full h-full"></div>
    </div>
  );
};
