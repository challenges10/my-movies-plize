export const Footer: React.FC = () => {
  return (
    <footer className="bg-[#0000009c]">
      <div className="container mx-auto h-14 flex justify-center items-center">
        <p className="text-[#ffffffbd] text-sm">
          <span className="italic font-bold"> My Movies</span>{" "}
          <span>Copyright © 2023 </span>
        </p>
      </div>
    </footer>
  );
};
