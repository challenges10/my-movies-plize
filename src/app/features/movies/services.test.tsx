import { ApiProvider } from "@reduxjs/toolkit/dist/query/react";
import { moviesApi, useGetMovieDetailQuery, useGetPopularMoviesQuery, useSearchMoviesByTitleQuery } from "./services";
import { act, render, screen, waitFor } from "@testing-library/react";
import { renderWithContext } from "../../../test-utils";

test('should define function MoviesApi', () => {
  expect(moviesApi.reducerPath).toEqual('moviesApi');
  expect(Object.keys(moviesApi.endpoints)).toEqual([
    "getPopularMovies",
    "getMovieDetail",
    "searchMoviesByTitle",
  ]);
});

test('should define MoviesApi list', async () => {
  const Dumb = () => {
    const { isLoading } = useGetPopularMoviesQuery();
    return <div>{isLoading ? 'IS_LOADING' : 'DATA_RECEIVED'}</div>;
  };

  act(() => {
    renderWithContext(
      <ApiProvider api={moviesApi}>
        <Dumb />
      </ApiProvider>
    );
  });

  await screen.findByText('DATA_RECEIVED');
  expect(screen.getByText('DATA_RECEIVED')).toBeDefined();
});


test('should define MoviesApi detail', async () => {
  const Dumb = () => {
    const { isLoading } = useGetMovieDetailQuery("1");
    return <div>{isLoading ? 'IS_LOADING' : 'DATA_RECEIVED'}</div>;
  };

  act(() => {
    renderWithContext(
      <ApiProvider api={moviesApi}>
        <Dumb />
      </ApiProvider>
    );
  });

  await screen.findByText('DATA_RECEIVED');
  expect(screen.getByText('DATA_RECEIVED')).toBeDefined();
});

test('should define MoviesApi search', async () => {
  const Dumb = () => {
    const { isLoading } = useSearchMoviesByTitleQuery("1");
    return <div>{isLoading ? 'IS_LOADING' : 'DATA_RECEIVED'}</div>;
  };

  act(() => {
    renderWithContext(
      <ApiProvider api={moviesApi}>
        <Dumb />
      </ApiProvider>
    );
  });

  await screen.findByText('DATA_RECEIVED');
  expect(screen.getByText('DATA_RECEIVED')).toBeDefined();
});