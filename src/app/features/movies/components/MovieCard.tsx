import { Movie } from "../types";
import { useNavigate } from "react-router-dom";
import { getImageUrl } from "../utils";

export type MovieCardProps = {
  movie: Movie;
};

export const MovieCard: React.FC<MovieCardProps> = ({ movie }) => {
  const navigate = useNavigate();

  const handleMovieCardClick = () => {
    navigate("/movie/" + movie.id);
  };

  return (
    <div
      className="rounded-lg overflow-hidden relative cursor-pointer hover:shadow-lg transition-shadow"
      onClick={handleMovieCardClick}
      data-testid="movie-card"
    >
      <h5 data-testid={`movie-title-${movie.id}`} className="text-white font-semibold text-lg absolute z-10 bottom-4 left-3">
        {movie.original_title}
      </h5>
      <img
        srcSet={
          getImageUrl(movie.backdrop_path || movie.poster_path, "w780") ||
          "https://via.placeholder.com/780x440"
        }
        alt={movie.original_title}
        className="scale-125 xl:scale-125 hover:scale-150 xl:hover:scale-100 ease-in duration-500 h-[258px] w-[780px] object-cover"
      />
    </div>
  );
};
