import React from "react";
import { fireEvent, screen } from "@testing-library/react";
import { renderWithContext } from "../../../../test-utils";
import { MovieCard, MovieCardProps } from "./MovieCard";
import { useNavigate } from "react-router-dom";

import mockMovieJson from "../../../../../public/movie.json";

const mockMovie: MovieCardProps = {
  movie: mockMovieJson,
};
jest.mock('react-router-dom', () => ({
  ...jest.requireActual('react-router-dom'),
  useNavigate: jest.fn(),
}));

const mockUseNavigate = useNavigate as jest.Mock;

beforeEach(() => {
  mockUseNavigate.mockClear();
});
test("Movie card should be listed", async () => {
  mockUseNavigate.mockImplementation(() => jest.fn());
  renderWithContext(<MovieCard movie={mockMovie.movie} />);
  const element = screen.getByTestId('movie-card')
  fireEvent.click(element);
  expect(element).toBeInTheDocument()
});