import React from "react";
import { Movie } from "../types";
import { MovieCard } from "./MovieCard";

type MoviesListGridProps = {
  movies?: Movie[];
  isLoading?: boolean;
  error?: any;
};

export const MoviesListGrid: React.FC<MoviesListGridProps> = ({
  movies,
  error,
  isLoading,
}) => {

  if (error) {
    return <div data-testid="error">"error" </div>;
  }

  return (
    <div className="grid xs:grid-cols-2 md:grid-cols-2 lg:grid-cols-3 gap-6 my-4">
      {isLoading
        ? "..."
        : movies?.map((movie) => <MovieCard key={movie.id} movie={movie} />)}
    </div>
  );
};
