import React from "react";
import { render, screen } from "@testing-library/react";
import { MoviesListGrid } from "./MoviesListGrid";

import mockMovieJson from "../../../../../public/movies.json";
import { useNavigate } from "react-router-dom";

const mockMovies = mockMovieJson.results;

jest.mock('react-router-dom', () => ({
  ...jest.requireActual('react-router-dom'),
  useNavigate: jest.fn(),
}));

const mockUseNavigate = useNavigate as jest.Mock;

beforeEach(() => {
  mockUseNavigate.mockClear();
});

describe("MoviesListGrid component", () => {
  it("should display a spinner while loading", () => {
    render(<MoviesListGrid movies={[]} isLoading={true} />);

    expect(screen.getByText("...")).toBeDefined();
  });

  it("should display an error message if there is an error", () => {
    render(<MoviesListGrid error={true} />);
    expect(screen.getByTestId("error")).toBeDefined();
  });

  it("should display a list of movies", () => {
    render(<MoviesListGrid movies={mockMovies} />);

    mockMovies.forEach((movie) => {
      expect(screen.getByTestId(`movie-title-${movie.id}`).textContent).toEqual(movie.original_title);
    });
  });
});