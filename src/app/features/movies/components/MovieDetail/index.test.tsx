import { fireEvent, render, screen } from "@testing-library/react";
import { MovieDetail, MovieDetailProps } from ".";

import mockMovieJson from "../../../../../../public/movieDetail.json";
import { renderWithContext } from "../../../../../test-utils";
import { useNavigate } from "react-router-dom";

const mockMovie: MovieDetailProps = {
  movie: mockMovieJson,
};

jest.mock('react-router-dom', () => ({
  ...jest.requireActual('react-router-dom'),
  useNavigate: jest.fn(),
}));

const mockUseNavigate = useNavigate as jest.Mock;


beforeEach(() => {
  mockUseNavigate.mockClear();
});

describe("MovieDetail component", () => {
  it("renders correctly", () => {
    mockUseNavigate.mockImplementation(() => jest.fn());
    renderWithContext(<MovieDetail movie={mockMovie.movie} />);
    const element = screen.getByTestId("data-return");
    fireEvent.click(element);
    expect(mockUseNavigate).toHaveBeenCalledTimes(1);
    expect(element).toBeInTheDocument();
  });
});