import { ArrowLeft } from "react-feather";
import { MovieDetail as MovieDetailType } from "../../types";
import { useNavigate } from "react-router-dom";
import { getImageUrl, printDuration } from "../../utils";
import { Voting } from "./Voting";

export type MovieDetailProps = {
  movie: MovieDetailType;
};

export const MovieDetail: React.FC<MovieDetailProps> = ({ movie }) => {

  const navigate = useNavigate();

  const handleReturnClick = () => {
    navigate(-1);
  };

  return (
    <div className="py-4">
      <div className="bg-[#324077] rounded-lg flex flex-col sm:flex-row shadow-md relative">
        <div
          className="absolute text-white font-bold top-4 left-4 cursor-pointer p-2 rounded-full hover:bg-[#ffffff18] bg-[#7777776c]"
          onClick={handleReturnClick} data-testid="data-return"
        >
          <ArrowLeft />
        </div>
        <div className="rounded-tl-lg rounded-tr-lg sm:rounded-tr-none sm:rounded-bl-lg overflow-hidden min-w-full sm:min-w-[342px] max-h-[300px] sm:max-h-[500px]">
          {movie.poster_path && (
            <img
              srcSet={getImageUrl(movie.poster_path, "w500")}
              alt={movie?.original_title}
              className="w-full max-h-full"
            />
          )}
        </div>
        <div className="p-6 text-white">
          <h3 className="text-3xl md:text-4xl font-bold pb-1" data-testid="data-title">
            {movie?.title}
          </h3>
          <div className="pb-4 space-x-4">
            <span className="text-xs font-extrabold">{movie.release_date}</span>
            <span className="text-xs font-extrabold">
              {printDuration(movie.runtime)}
            </span>
          </div>
          <h6 className="text-lg font-semibold pb-2">{movie.tagline}</h6>
          <div className="flex pb-6 flex-wrap">
            {movie?.genres?.map((genre) => (
              <span
                key={`genre-${genre.id}`}
                className="cursor-pointer flex text-sm text-white rounded-sm bg-green-700 hover:bg-green-800 px-3 mr-2 mb-1"
              >
                {genre.name}
              </span>
            ))}
          </div>
          <p className="text-sm pb-3">{movie.overview}</p>
          <div>
            <p className="font-bold">Voting</p>
            <div className="flex items-center space-x-1">
              <Voting voting={movie.vote_average / 2} />{" "}
              <span>({movie.vote_count})</span>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};
