import { Star } from "react-feather";

type VotingProps = {
  voting?: number;
};

export const Voting: React.FC<VotingProps> = ({ voting }) => {
  return (
    <div className="flex items-center space-x-1 py-1">
      {[0, 1, 2, 3, 4].map((rat) => (
        <Star
          key={rat}
          fill={voting && voting > rat ? "#ee9b00" : "white"}
          className={`${
            voting && voting > rat ? "text-[#ee9b00]" : "text-white"
          } h-5 w-5 flex-shrink-0`}
          aria-hidden="true"
        />
      ))}
    </div>
  );
};
