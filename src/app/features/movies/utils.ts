export const printDuration = (minutes?: number) => {
  if (!minutes) return "";
  const hours = minutes / 60;
  const rhours = Math.floor(hours);
  const min = (hours - rhours) * 60;
  const rminutes = Math.round(min);
  return rhours + "h " + rminutes + "min";
};

export const getImageUrl = (imgPath: string, size = "w300") =>
  imgPath ? `${process.env.REACT_APP_MOVIE_IMG_URL}${size}${imgPath}` : "";
