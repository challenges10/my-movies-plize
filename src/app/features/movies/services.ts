import { createApi } from "@reduxjs/toolkit/query/react";
import { fetchBaseQuery } from "@reduxjs/toolkit/dist/query";
import type { Movie, MovieDetail } from "./types";

export type GetPopularMovies = {
  page: number;
  results: Movie[];
  total_pages: number;
  total_results: number;
};

const baseQuery = fetchBaseQuery({
  baseUrl: process.env.REACT_APP_MOVIES_API_URL,
});

const apiKeyParam = { api_key: process.env.REACT_APP_MOVIES_API_KEY };

export const moviesApi = createApi({
  reducerPath: "moviesApi",
  baseQuery,
  endpoints: (builder) => ({
    getPopularMovies: builder.query<GetPopularMovies, void>({
      query: () => ({
        url: "movie/popular",
        params: apiKeyParam,
      }),
    }),
    getMovieDetail: builder.query<MovieDetail, string>({
      query: (id) => ({
        url: "movie/" + id,
        params: apiKeyParam,
      }),
    }),
    searchMoviesByTitle: builder.query<GetPopularMovies, string>({
      query: (title) => ({
        url: "search/movie",
        params: { query: title, ...apiKeyParam },
      }),
    }),
  }),
});

export const {
  useGetPopularMoviesQuery,
  useGetMovieDetailQuery,
  useSearchMoviesByTitleQuery,
} = moviesApi;
