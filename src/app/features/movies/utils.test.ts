import { getImageUrl, printDuration } from './utils';

describe('printDuration', () => {
  it('should return empty string when minutes is not defined', () => {
    const result = printDuration();
    expect(result).toBe('');
  });

  it('should return correct duration string', () => {
    const result = printDuration(120);
    expect(result).toBe('2h 0min');
  });
});

describe('getImageUrl', () => {
  it('should return default image URL when imgPath is not defined', () => {
    const result = getImageUrl('');
    expect(result).toBe('');
  });

  it('should return correct image URL with size w300', () => {
    const result = getImageUrl('/path/to/img', 'w300');
    expect(result).toBe(`${process.env.REACT_APP_MOVIE_IMG_URL}w300/path/to/img`);
  });

  it('should return correct image URL with default size w300', () => {
    const result = getImageUrl('/path/to/img');
    expect(result).toBe(`${process.env.REACT_APP_MOVIE_IMG_URL}w300/path/to/img`);
  });
});

