import React from "react";
import App from "./App";
import { renderWithContext } from "./test-utils";

describe("App component", () => {
  it("renders the root route", async () => {
    renderWithContext(<App />);
  });
});
