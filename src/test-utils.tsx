import { Provider } from "react-redux";
import { BrowserRouter as Router } from "react-router-dom";
import React from "react";
import { render } from "@testing-library/react";
import { RootState, store } from "./app/store";

export function renderWithContext(
    element: React.ReactElement,
    state?: RootState
) {
    const utils = render(
        <Provider store={store}>
            <Router>{element}</Router>
        </Provider>
    );
    return { store, ...utils };
}