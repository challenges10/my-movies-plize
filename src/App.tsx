import React from "react";
import { Route, Routes } from "react-router-dom";
import { MoviesListPage } from "./app/pages/MoviesListPage";
import { MoviesDetailPage } from "./app/pages/MovieDetailPage";

function App() {
  return (
    <Routes>
      <Route path={"/"} element={<MoviesListPage />} />
      <Route path={"/movie/:movieId"} element={<MoviesDetailPage />} />
    </Routes>
  )
}

export default App;
