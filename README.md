# My Movies

This a simple React website that fetch and display popular movies from [themoviedb.org](https://themoviedb.org).

## Installation

1. Install the dependencies:

```bash
npm install
```

2. Create a new file named `.env` by copying the `env.example` file:

```bash
cp env.example .env
```

## Running the Project

Start the development server:

```bash
npm start
```

## Running the test

Start the test:

```bash
npm run test
```

## Running the test with coverage

Start the test with coverage:

```bash
npm run test:cov
```

## Building the Project

Start the development server:

```bash
npm run build
```

This will create a build directory with the production-ready version of the project.
